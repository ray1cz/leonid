/*
 * Copyright (C) 2021 Rene Lauer (ray@phalanx.cz)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.phalanx.leonid.handlers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

/**
 *	A handler for the shutdown of the server.
 * @author Rene Lauer (ray@phalanx.cz)
 */
public class ShutdownHandler implements HttpRequestHandler
{

	private final CountDownLatch latch;
	private final String page;

	public ShutdownHandler()
	{
		this.latch = new CountDownLatch(1);
		this.page = "<html>\n" +
					" <head>\n" +
					"  <title>Leonid HTTP Server Shut Down</title>\n" +
					"  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
					"</head>\n" +
					" <body>\n" +
					"  <h1>Leonid server has been shut down.</h1>\n" +
					"  <p>\n" +
					"	Thanks for using, bye! :-)\n" +
					" </p>\n" +
					" </body>\n" +
					"</html>";
	}

	@Override
	public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws HttpException, IOException
	{
		StringEntity entity = new StringEntity(page, ContentType.create("text/html", StandardCharsets.UTF_8));
		response.setStatusCode(200);
		response.setEntity(entity);
		this.latch.countDown();
	}

	public CountDownLatch getLatch()
	{
		return this.latch;
	}
	
}
