/*
 * Copyright (C) 2021 Rene Lauer (ray@phalanx.cz)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.phalanx.leonid.handlers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.MimeType;
import javax.activation.MimeTypeParameterList;
import javax.activation.MimeTypeParseException;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

/**
 *	This class statically handles the request for contents of a specified file. The path, URI and mime types are statically stored.
 * @author Rene Lauer (ray@phalanx.cz)
 */
public class ContentHandler implements HttpRequestHandler
{

	private static final Logger LOG = Logger.getLogger(ContentHandler.class.getName());

	private final String extension;
	private String mimeType;
	private final Path name;
	private final Path path;
	private final UUID id;
	private NameValuePair[] parameters;
	private final boolean attach;

	public ContentHandler(String ext, String mime, Path fileName, Path f, boolean attach)
	{
		this.extension = ext;
		this.name = fileName;
		this.path = f;
		this.attach = attach;
		this.id = UUID.randomUUID();
		try
		{
			MimeType mt = new MimeType(mime);
			this.mimeType = mt.getBaseType();
			MimeTypeParameterList params = mt.getParameters();
			if (params != null)
			{
				Enumeration<String> names = params.getNames();
				ArrayList<NameValuePair> pairs = new ArrayList<>();
				while (names.hasMoreElements())
				{
					String key = names.nextElement();
					String value = params.get(key);
					pairs.add(new BasicNameValuePair(key, value));
				}
				this.parameters = pairs.stream().toArray(NameValuePair[]::new);
			}
			else
				this.parameters = null;
		}
		catch (MimeTypeParseException ex)
		{
			LOG.log(Level.SEVERE, "Mime type {0} is unparsable, replacing with application/octet-stream", mime);
			this.mimeType = "application/octet-stream";
			this.parameters = null;
		}
	}

	@Override
	public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws HttpException, IOException
	{
		if (Files.exists(path) && Files.isRegularFile(path) && Files.isReadable(path))
		{
			FileEntity entity;
			if (this.parameters != null)
				entity = new FileEntity(path.toFile(), ContentType.create(mimeType, parameters));
			else
				entity = new FileEntity(path.toFile(), ContentType.create(mimeType));
			response.setStatusCode(200);
			if (this.attach)
				response.addHeader("Content-Disposition", "attachment; filename=\""+this.getName()+"\"");
			response.setEntity(entity);
		}
		else
			response.setStatusCode(404);
	}

	public String getUri()
	{
		return "/"+id.toString();
	}

	public String getPath()
	{
		return this.path.toString();
	}

	public String getExtension()
	{
		return extension;
	}

	public String getMimeType()
	{
		return mimeType;
	}

	public String getName()
	{
		return name.toString();
	}
}
