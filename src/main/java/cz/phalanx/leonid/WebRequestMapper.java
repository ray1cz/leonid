/*
 * Copyright (C) 2021 Rene Lauer (ray@phalanx.cz)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.phalanx.leonid;

import java.io.IOException;
import java.util.HashMap;
import java.util.stream.Stream;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.http.protocol.UriHttpRequestHandlerMapper;

/**
 *	A class that maps HTTP requests to their respectve handlers.
 * @author Rene Lauer (ray@phalanx.cz)
 */
public class WebRequestMapper extends UriHttpRequestHandlerMapper
{
	private final HashMap<String, HttpRequestHandler> registrations;
	private final HttpRequestHandler error404;
	
	public WebRequestMapper()
	{
		super();
		this.registrations = new HashMap<>();
		error404 = new HttpRequestHandler()
		{
			@Override
			public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws HttpException, IOException
			{
				response.setStatusCode(HttpStatus.SC_NOT_FOUND);
				response.setReasonPhrase("Not Found");
				response.setEntity(new StringEntity("Requested URI not found"));
			}
		};
	}

	@Override
	public void unregister(String pattern)
	{
		super.unregister(pattern); 
		this.registrations.remove(pattern);
	}

	@Override
	public void register(String pattern, HttpRequestHandler handler)
	{
		super.register(pattern, handler); 
		if (handler instanceof HttpRequestHandler)
			this.registrations.put(pattern, (HttpRequestHandler) handler);
	}

	@Override
	public HttpRequestHandler lookup(HttpRequest request)
	{
		HttpRequestHandler retval = super.lookup(request); 
		if (retval == null)
			return this.error404;
		else
			return retval;
	}
	
	public Stream<String> registeredUris()
	{
		return registrations.keySet().stream();
	}
	
	public HttpRequestHandler getHandler(String uri)
	{
		return this.registrations.get(uri);
	}
}
