/*
 * Copyright (C) 2021 Rene Lauer (ray@phalanx.cz)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.phalanx.leonid;

import cz.phalanx.leonid.handlers.ContentHandler;
import cz.phalanx.leonid.handlers.RootHandler;
import cz.phalanx.leonid.handlers.ShutdownHandler;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.apache.http.ExceptionLogger;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.bootstrap.HttpServer;
import org.apache.http.impl.bootstrap.ServerBootstrap;
import org.apache.http.impl.client.DefaultClientConnectionReuseStrategy;
import org.apache.http.protocol.HttpProcessor;
import org.apache.http.protocol.HttpProcessorBuilder;
import org.phalanx.rclib.anno.AutoConfig;
import org.phalanx.rclib.anno.Option;
import org.phalanx.rclib.config.CliConfigurator;
import org.apache.http.protocol.RequestConnControl;
import org.apache.http.protocol.RequestContent;
import org.apache.http.protocol.RequestTargetHost;
import org.apache.http.protocol.ResponseConnControl;
import org.apache.http.protocol.ResponseContent;
import org.apache.http.protocol.ResponseDate;
import org.apache.http.protocol.ResponseServer;

/**
 *	This is a micro-http server with a single purpose: to serve the files over HTTP.
 * 
 * @author Rene Lauer (ray@phalanx.cz)
 */
@AutoConfig(validate = true)
public class Main implements Runnable
{
	private static final Logger LOG = Logger.getLogger(Main.class.getName());

	@Option(value = "??", defval = "/usr/local/elza/packages")
	private String[] directories;
	
	@Option("p")
	private int port;
	
	@NotNull
	@Size(min = 1)
	@Option(value = "e", defval = "zip")
	private String[] extensions;
	
	@NotNull
	@Size(min = 1)
	@Option(value="t", defval = "application/zip")
	private String[] mimeTypes;
	
	@Option("a")
	private boolean attach;
	
	/* Non-confgurable variables */
	private final WebRequestMapper requestMapper;
	private final ServerBootstrap bootstrap;
	private HttpServer server;
	
	public Main()
	{
		this.requestMapper = new WebRequestMapper();
		SocketConfig socketConfig = SocketConfig.DEFAULT;
		ExceptionLogger logger = ex -> {
			if (! (ex instanceof org.apache.http.ConnectionClosedException))
				Main.LOG.log(Level.SEVERE, "Server encountered an exception", ex);
		};
		HttpProcessorBuilder procBuilder = HttpProcessorBuilder.create();
		procBuilder.addAll(new RequestConnControl(), new RequestContent(true), new RequestTargetHost());
		procBuilder.addAll(new ResponseConnControl(), new ResponseContent(true), new ResponseDate(), new ResponseServer());
		HttpProcessor processor = procBuilder.build();
		bootstrap = ServerBootstrap.bootstrap();
		bootstrap.setConnectionReuseStrategy(new DefaultClientConnectionReuseStrategy());
		bootstrap.setExceptionLogger(logger);
		bootstrap.setHttpProcessor(processor);
		bootstrap.setServerInfo("Leonid 1.0");
		bootstrap.setSocketConfig(socketConfig)
			.setHandlerMapper(requestMapper);
	}
	

	public static void main(String[] args) 
	{
		CliConfigurator<Main> configurator = new CliConfigurator<>(Main.class, false);
		try
		{
			Main self = configurator.getConfiguredInstance(args);
			self.run();
		}
		catch (Throwable ex)
		{
			LOG.log(Level.SEVERE, "Error running Leonid", ex);
			System.exit(1);
		}
	}

	@Override
	public void run()
	{
		if (this.directories == null)
			throw new IllegalArgumentException("No directories to scan");
		final Map<String, String> typeMap = this.makeTypeMap(this.extensions, this.mimeTypes);
		List<Path> paths = Arrays.stream(this.directories).map(Paths::get).filter(Files::exists).filter(Files::isDirectory).filter(Files::isReadable).collect(Collectors.toList());
		if (paths.isEmpty())
		{
			LOG.warning("No suitable directories to serve, exiting.");
			return;
		}
		List<ContentHandler> handlers = paths.stream().flatMap(p ->
				{
					try
					{
						return Files.list(p);
					}
					catch (IOException ex)
					{
						return null;
					}
				})
				.filter(Objects::nonNull)
				.filter(Files::isRegularFile)
				.filter(Files::isReadable)
				.map(f -> {
					int ndx = f.getFileName().toString().lastIndexOf('.');
					if (ndx < 0)
						return null;
					else
					{
						String ext = f.getFileName().toString().substring(ndx+1);
						String mime = typeMap.get(ext);
						if (mime != null)
							return new ContentHandler(ext, mime, f.getFileName(), f, attach);
					}
					return null;
				})
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		RootHandler root = new RootHandler(handlers);
		ShutdownHandler down = new ShutdownHandler();
		this.requestMapper.register("/", root);
		this.requestMapper.register("/exit", down);
		handlers.stream()
				.peek(ch -> LOG.log(Level.INFO, "Serving {0} as {1}", new Object[]{ch.getPath(), ch.getUri()}))
				.forEach(ch -> this.requestMapper.register(ch.getUri(), ch));
		CountDownLatch block = down.getLatch();
		
		try
		{
			this.start();
			block.await();
			LOG.info("Leonid server shutting down on user request");
			this.server.shutdown(5, TimeUnit.SECONDS);
			this.server.awaitTermination(5, TimeUnit.SECONDS);
		}
		catch (IOException ex)
		{
			LOG.log(Level.SEVERE, "Could not start server", ex);
			System.exit(2);
		}
		catch (InterruptedException ex)
		{
			LOG.log(Level.INFO, "Interrupted, exiting.");
			System.exit(0);
		}
	}
	
	private void start() throws IOException
	{
		if (this.port >= 0)
			bootstrap.setListenerPort(port);		
		this.server = bootstrap.create();
		this.server.start();
		LOG.log(Level.INFO, "Leonid server listening on port {0}", this.server.getLocalPort());
		Runtime.getRuntime().addShutdownHook(new Thread (() -> {
			this.server.shutdown(5, TimeUnit.SECONDS);
			LOG.log(Level.INFO, "Server stopped ");
		}));
	}
	
	private Map<String, String> makeTypeMap(String[] exts, String[] mimes)
	{
		Objects.requireNonNull(exts, "No extensions to scan");
		if (exts.length < 1)
			throw new IllegalArgumentException("No extensions specified");
		final String[] types = mimes == null ? new String[0] : mimes;
		if (types.length < 1)
			LOG.warning("No MIME types have been specified, all contents will be served as application/octet-stream");
		return IntStream.range(0, exts.length).collect(
				() -> new TreeMap<>(String.CASE_INSENSITIVE_ORDER),
				(map, i) -> map.put(exts[i], i >= types.length ? "application/octet-stream" : types[i]), 
				(a, b) -> a.putAll(b));
	}
}
