The server currently depends on a library that is (not yet) publicly available, but if you need the functionality, go ahead and download the leonid.jar. It's been compiled for Java 1.8 and will NOT work
on higher versions out of the box as it relies on javax.activation.MimeType.

To run the server, just use java -jar leonid.jar [-a] [-e ext -t mime]* [-p port] dir ...
Command-line arguments:
-a send files as attachments (add Content-Disposition: attachment)
-e extension - specify extensions of files to include (e.g. -e zip). Each -e must be followed by -t
-t mimetype - specify mime type reported for the corresponding extension (e.g. -e jpeg -t image/jpeg)
-p port - listen on the specified port. If not specified, a random port will be chosen
dir ... - list of directories to scan and serve

The -e and -t must repeat in pairs (see above). If you don't include any, the default is to serve zip files as application/zip only.

The software is provided as is and I shall not be held liable for any damages caused by its use. YOU USE IT AT YOUR OWN RISK.
Enjoy.